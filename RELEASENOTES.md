# RELEASE NOTES: *kserver*, Prototype of a bi-threaded server to be used as test vehicule for 'kong' application..

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.1.5**:
  - Updated build system components.

- **Version 1.1.4**:
  - Updated build system.

- **Version 1.1.3**:
  - Removed unused files.

- **Version 1.1.2**:
  - Updated build system component(s)

- **Version 1.1.1**:
  - Reworked build system to ease global and inter-project updated.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.1.0**:
  - Added new data reporting client.

- **Version 1.0.0**:
  - First version.
