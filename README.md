 # *kserver*, prototype of a bi-threaded server to be used as test vehicule for 'kong' application.

***...TO BE COMPLETED...***

``` text
                                     HOST #1                 HOST #2              HOST #3...
                 Predefined
  --------------  Protocol
  |            | Modbus/TCP
  | Sensor #0  |===========>|
  |            | Modbus/RTU |
  --------------            |
                            |                   Predefined
                            |                    Protocol
                            |     -------------   over    ------------          ---------------
                            |     |           |  TCP/IP   |          |  TCP/IP  |   Report    |
  ..............            |====>| Automaton |==========>|  Server  |<========>|  Generator  |---> output
                            |     |           |           |          |          | Instance #p |
                            |     -------------           ------------          ---------------
                            |                                  ^
                            |                                  |
  --------------            |                                  v
  |            |            |                             /----------\
  | Sensor #nn |===========>|                             |  SQLite  |
  |            |                                          | Database |
  --------------                                          \----------/

<==================================================> <======================> <====================================>
  Protoypé par un "client" C générant des données   | Prototypé par un       | Prototypé par un client "C"
  aléatoirement pour 16 capteurs (0..15).           | serveurC multithreadé. | 
  Ces données sont :                                | Un thread pour l'écoute| 
	long time: 32 bits déterminant un temps         | de l'automate et un    |
	           depuis le 01/01/1977 (unix)          | second pour l'écoute   |
	float value: valeur réelle égale à              | des différentes ins-   |
	           PI multiplié par le numéro           | -tances du générateur  |
	           du capteur + 1                       | de rapports.           |
	unsigned int crc: pour "cyclic redundancy       | Les données envoyées   |
	           check".                              | par l'automate sont    |
	                                                | stockées dans une base |
	                                                | SQLite3 (free) à       |
	                                                | raison d'une table par |
	                                                | capteur.               |
 ==> kautomaton                                       ==> kserver              ==> kreport & kreport2
```

![Example](README_images/kong01.png  "Example 1")

![Example](README_images/kong02.png  "Example 2")

## LICENSE
**kserver** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ kserver -h
kserver - Copyright (c) 2015, 2020, Michel Rizzo. All Rights Reserved.
kserver - Version 1.0.0

Prototype of a bi-threaded server to be used as test vehicule for 'kong'
application.

Usage: kserver [OPTIONS]...

  -h, --help                    Print help and exit
  -V, --version                 Print version and exit
  -d, --database=SQLITE DATABASE
                                Name of the SQLite data base
  -l, --log=LOG DIRECTORY       Directory of log file

Exit: returns a non-zero status if an error is detected.

$ kserver -V
kserver - Copyright (c) 2015, 2020, Michel Rizzo. All Rights Reserved.
kserver - Version 1.0.0
$ kserver -d ~/tmp/db
....
$ kautomaton -h
kautomaton - Copyright (c) 2015, 2020, Michel Rizzo. All Rights Reserved.
kautomaton - Version 1.0.0

Prototype of a bi-threaded server to be used as test vehicule for 'kong'
application.

Usage: kautomaton [OPTIONS]...

  -h, --help     Print help and exit
  -V, --version  Print version and exit
  -v, --verbose  Verbose mode  (default=off)
$ kautomaton -V
kautomaton - Copyright (c) 2015, 2020, Michel Rizzo. All Rights Reserved.
kautomaton - Version 1.0.0
$ kautomaton -v
...Sensor 13: Send record 0 (1583686426, 43.982400)
...Sensor 6: Send record 0 (1583686428, 21.991200)
...Sensor 12: Send record 0 (1583686430, 40.840800)
....
$ kreport -h
kreport - Copyright (c) 2015, 2020, Michel Rizzo. All Rights Reserved.
kreport - Version 1.0.0

Prototype of a bi-threaded server to be used as test vehicule for 'kong'
application.

Usage: kreport [OPTIONS]...

  -h, --help     Print help and exit
  -V, --version  Print version and exit
  -v, --verbose  Verbose mode  (default=off)
$ kreport -V
kreport - Copyright (c) 2015, 2020, Michel Rizzo. All Rights Reserved.
kreport - Version 1.0.0
$ kreport -v
...Request values for sensor tables
-------------------------
| TABLES   | ROWS       |
-------------------------
| SENSOR00 |        466 |
| SENSOR01 |        487 |
| SENSOR02 |        527 |
| SENSOR03 |        463 |
| SENSOR04 |        492 |
| SENSOR05 |        456 |
| SENSOR06 |        466 |
| SENSOR07 |        495 |
| SENSOR08 |        478 |
| SENSOR09 |        489 |
| SENSOR10 |        509 |
| SENSOR11 |        448 |
| SENSOR12 |        501 |
| SENSOR13 |        484 |
| SENSOR14 |        481 |
| SENSOR15 |        475 |
-------------------------
...Request values for sensor 13
--------------------------------------------------------------
| Sensor 13                                                  |
--------------------------------------------------------------
| 00000 | 1583508743 | Fri Mar  6 16:32:23 2020 |  43.982400 |
| 00001 | 1583508868 | Fri Mar  6 16:34:28 2020 |  43.982400 |
| 00002 | 1583513149 | Fri Mar  6 17:45:49 2020 |  43.982400 |
| 00003 | 1583513440 | Fri Mar  6 17:50:40 2020 |  43.982400 |
| 00004 | 1583513817 | Fri Mar  6 17:56:57 2020 |  43.982400 |
| 00005 | 1583567238 | Sat Mar  7 08:47:18 2020 |  43.982400 |
....
```
## STRUCTURE OF THE APPLICATION
This section walks you through **kserver**'s structure. Once you understand this structure, you will easily find your way around in **kserver**'s code base.

``` bash
$ yaTree
./                             # Application level
├── README_images/             # 
│   ├── kong01.png             # 
│   └── kong02.png             # 
├── src/                       # Source directory
│   ├── automaton/             # Example of simple data acquisition application
│   │   ├── Makefile           # Makefile
│   │   ├── kautomaton.c       # Main source file
│   │   └── kautomaton.ggo     # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
│   ├── database/              # Database creation
│   │   ├── DBcreate.c         # Main source file
│   │   ├── DBcreate.ggo       # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
│   │   └── Makefile           # Makefile
│   ├── reporting/             # Example of simple data reporting application
│   │   ├── Makefile           # Makefile
│   │   ├── kreport.c          # Main source file
│   │   └── kreport.ggo        # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
│   ├── reporting2/            # A second example of simple data reporting application
│   │   ├── kreport2/          # 
│   │   │   ├── Makefile       # Makefile
│   │   │   ├── kreport2.c     # Main source file
│   │   │   ├── kreport2.ggo   # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
│   │   │   └── kreport2.h     # Header file
│   │   ├── kreport2nil/       # 
│   │   │   ├── Makefile       # Makefile
│   │   │   └── kreport2nil.c  # Main program source
│   │   └── Makefile           # 
│   ├── servcli/               # Sercer/Client Common routines
│   │   ├── Makefile           # Makefile
│   │   ├── double_swap.c      # 
│   │   ├── fatal.c            # 
│   │   ├── kservcli.h         # 
│   │   ├── log.c              # 
│   │   ├── randinterval.c     # 
│   │   ├── serverIF.c         # 
│   │   ├── threadSleep.c      # 
│   │   └── warning.c          # 
│   ├── server/                # Bi-threaded Server
│   │   ├── Makefile           # Makefile
│   │   ├── kacqthread.c       # Data acquisition source
│   │   ├── krepthread.c       # Data reporting source
│   │   ├── kserver.c          # Main source file
│   │   ├── kserver.ggo        # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
│   │   ├── kserver.h          # Header file for co-applications
│   │   └── kserver_internal.h # Local header file
│   └── Makefile               # Makefile
├── COPYING.md                 # GNU General Public License markdown file
├── LICENSE.md                 # License markdown file
├── Makefile                   # Makefile
├── README.md                  # ReadMe markdown file
├── RELEASENOTES.md            # Release Notes markdown file
└── VERSION                    # Version identification text file

10 directories, 41 files
$ 
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd kserver
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION
```Shell
$ cd kserver
$ make release
    # Executables generated with -O2 option, and the header file, are respectively installed in $BIN_DIR  and $INC_DIR directory (defined at environment level). We consider that $BIN_DIR is a part of the PATH.
```

## NOTES

## SOFTWARE REQUIREMENTS
- For usage, nothing particular...
- For development:
   - *GengetOpt* binary package installed, version 2.22.6.
- Developped and tested on XUBUNTU 19.04, GCC v8.3.0

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***