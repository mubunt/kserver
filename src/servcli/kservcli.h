//------------------------------------------------------------------------------
// Copyright (c) 2015, 2020, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kserver
// Prototype of a bi-threaded server to be used as test vehicule for 'kong' application.
//------------------------------------------------------------------------------
#ifndef KSERVCLI_H
#define KSERVCLI_H
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define SERVER_BYDEFAULT				"127.0.0.1"
#define PORT_AUTOMATON					4321
#define PORT_REPORTER					6543

#define MAXSERVERNAME					128
#define MAXCOMMANDSIZE					64

#define NOMORESENSOR					999999
//--- Common commands to dialog with the server --------------------------------
#define SERVER_DISCONNECT				64
//--- Automaton Commands to dialog with the server -----------------------------
#define A_VALUES_SENT					1
//--- Reporter Commands to dialog with the server ------------------------------
#define R_VALUES_REQUESTED				1
#define R_TABLES_REQUESTED				2
//--- Table management ---------------------------------------------------------
#define MAXTABLENAME					12
#define TABLENAMEFORMAT					"SENSOR%02d"

#define SOCKET							int
#define	closesocket						close
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
struct __attribute__((aligned(4),packed)) Command_t {
	unsigned int	command;
};
struct __attribute__((aligned(4),packed)) A_Sensor_t {
	unsigned int	sensorId;
	__uint64_t		time;
	double			value;
	__uint64_t		crc;
};
struct __attribute__((aligned(4),packed)) R_Sensor_t {
	unsigned int	sensorId;
	__uint64_t		timestart;
	__uint64_t		timeend;
	__uint64_t		crc;
};
struct __attribute__((aligned(4),packed)) R_SensorSent_t {
	unsigned int	sensorId;
	__uint64_t		time;
	double			value;
	__uint64_t		crc;
};
struct __attribute__((aligned(4),packed)) R_TableSent_t {
	char			name[MAXTABLENAME];
	size_t			size;
	size_t			rows;
	__uint64_t		crc;
};
//------------------------------------------------------------------------------
// EXTERNAL FUNCTIONS
//------------------------------------------------------------------------------
extern void				_fatalError( const char *, ... );
extern void				_log( bool, const char *, ... );
extern void				_warning( const char *, ... );
extern void				_threadSleep( int32_t );
extern int				_connect2server( char *, u_int16_t, int * );
extern int				_closeServer( int );
extern int 				_sendCommand2server( int, unsigned int );
extern int				_sendCommand2server2( int, unsigned int, unsigned int, __uint64_t, double );
extern int 				_sendCommand2server3(int, unsigned int, unsigned int, __uint64_t, __uint64_t);
extern unsigned int 	_rand_interval( unsigned int, unsigned int );
extern double			_double_swap( double );
extern __uint64_t 		A_computeCRC( unsigned int, struct A_Sensor_t );
extern __uint64_t 		R_computeCRC( unsigned int, struct R_Sensor_t );
extern __uint64_t		R_computeTableCRC( struct R_TableSent_t );
extern __uint64_t		R_computeValueCRC( struct R_SensorSent_t );
//------------------------------------------------------------------------------
#endif	// KSERVCLI_H