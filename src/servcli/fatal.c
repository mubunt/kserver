//------------------------------------------------------------------------------
// Copyright (c) 2015, 2020, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kserver
// Prototype of a bi-threaded server to be used as test vehicule for 'kong' application.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>
#include <ctype.h>
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define FATALERROR_PREFIX	"FATAL ERROR: "
#define ESC_BOLDRED			"\033[1;31m"
#define ESC_STOP			"\033[0m"
//------------------------------------------------------------------------------
// MAIN ROUTINE
//------------------------------------------------------------------------------
void _fatalError(const char *msg, ...) {
	va_list al;
	char buff[512];

	strcpy(buff, "\n");
	strcat(buff, FATALERROR_PREFIX);
	va_start(al, msg);
	vsprintf(buff + strlen(FATALERROR_PREFIX) + 1, msg, al);
	strcat(buff, ".\n\n");
	va_end(al);
	fprintf(stderr, ESC_BOLDRED "%s" ESC_STOP, buff);
	exit(EXIT_FAILURE);
}
//------------------------------------------------------------------------------
