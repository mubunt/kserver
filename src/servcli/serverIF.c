//------------------------------------------------------------------------------
// Copyright (c) 2015, 2020, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kserver
// Prototype of a bi-threaded server to be used as test vehicule for 'kong' application.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <limits.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <ctype.h>
#include <stdbool.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//------------------------------------------------------------------------------
// LOCAL HEADER FILE
//------------------------------------------------------------------------------
#include "kservcli.h"
//------------------------------------------------------------------------------
// ROUTINES
//------------------------------------------------------------------------------
__uint64_t A_computeCRC( unsigned int command, struct A_Sensor_t sensoruplet ) {
	__uint64_t crc = (__uint64_t)command
	                 + (__uint64_t)sensoruplet.sensorId
	                 + sensoruplet.time
	                 + (__uint64_t)sensoruplet.value;
	return(crc);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
__uint64_t R_computeCRC( unsigned int command, struct R_Sensor_t sensoruplet) {
	__uint64_t crc = (__uint64_t)command
	                 + (__uint64_t)sensoruplet.sensorId
	                 + sensoruplet.timestart
	                 + sensoruplet.timeend;
	return(crc);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
__uint64_t R_computeTableCRC( struct R_TableSent_t table ) {
	__uint64_t crc = (__uint64_t)table.size + (__uint64_t)table.rows;
	for (int i = 0; i < MAXTABLENAME; i++) crc = crc + (__uint64_t)table.name[i];
	return(crc);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
__uint64_t R_computeValueCRC( struct R_SensorSent_t data ) {
	__uint64_t crc = (__uint64_t)data.sensorId
	                 + data.time
	                 + (__uint64_t)data.value;
	return(crc);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int _sendCommand2server( int socketServer, unsigned int command ) {
	struct Command_t cmd;
	cmd.command = htonl(command);
	if (0 >= send(socketServer, (void *)&cmd, sizeof(cmd), 0)) return(-1);
	return(0);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int _sendCommand2server2(int socketServer, unsigned int command, unsigned int sensor, __uint64_t time, double value) {
	struct Command_t cmd;
	struct A_Sensor_t sensoruplet;
	cmd.command = htonl(command);
	sensoruplet.sensorId = htonl(sensor);
	sensoruplet.time = htobe64(time);
	sensoruplet.value = _double_swap(value);
	sensoruplet.crc  = 0;	// To avoid warning from cppcheck!!!
	sensoruplet.crc = htobe64(A_computeCRC(cmd.command, sensoruplet));
	if (0 >= send(socketServer, (void *)&cmd, sizeof(cmd), 0)) return(-1);
	if (0 >= send(socketServer, (void *)&sensoruplet, sizeof(sensoruplet), 0)) return(-2);
	return(0);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int _sendCommand2server3(int socketServer, unsigned int command, unsigned int sensor, __uint64_t timestart,__uint64_t timeend) {
	struct Command_t cmd;
	struct R_Sensor_t sensoruplet;
	cmd.command = htonl(command);;
	sensoruplet.sensorId = htonl(sensor);
	sensoruplet.timestart = htobe64(timestart);
	sensoruplet.timeend = htobe64(timeend);
	sensoruplet.crc  = 0;	// To avoid warning from cppcheck!!!
	sensoruplet.crc = htobe64(R_computeCRC(cmd.command, sensoruplet));
	if (0 >= send(socketServer, (void *)&cmd, sizeof(cmd), 0)) return(-1);
	if (0 >= send(socketServer, (void *)&sensoruplet, sizeof(sensoruplet), 0)) return(-2);
	return(0);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int _connect2server( char *srv, uint16_t port, int *socketServer ) {
	struct sockaddr_in serveraddr;
	struct hostent *server;
	*socketServer = socket(AF_INET, SOCK_STREAM, 0);
	if (*socketServer < 0)  return(-3);
	server = (struct hostent *)gethostbyname(srv);
	if (server == NULL) return(-2);
	bzero((char *) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serveraddr.sin_addr.s_addr, (size_t)server->h_length);
	serveraddr.sin_port = htons(port);
	if (connect(*socketServer, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) < 0) {
		close(*socketServer);
		return(-1);
	}
	return(0);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int _closeServer( int socketServer ) {
	int rc = _sendCommand2server(socketServer, SERVER_DISCONNECT);
	closesocket(socketServer);
	return(rc);
}
//------------------------------------------------------------------------------
