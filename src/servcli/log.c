//------------------------------------------------------------------------------
// Copyright (c) 2015, 2020, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kserver
// Prototype of a bi-threaded server to be used as test vehicule for 'kong' application.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>
#include <ctype.h>
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define ESC_BOLDCYAN		"\033[1;36m"
#define ESC_STOP			"\033[0m"
//------------------------------------------------------------------------------
// MAIN ROUTINE
//------------------------------------------------------------------------------
void _log(bool log, const char *format, ...) {
	if (log) {
		va_list argp;
		char buff[512];
		va_start(argp, format);
		vsprintf(buff, format, argp);
		fprintf(stdout, ESC_BOLDCYAN "%s" ESC_STOP, buff);
		va_end(argp);
	}
}
//------------------------------------------------------------------------------
