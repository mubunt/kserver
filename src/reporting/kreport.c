//------------------------------------------------------------------------------
// Copyright (c) 2015, 2020, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kserver
// Prototype of a bi-threaded server to be used as test vehicule for 'kong' application.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <limits.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//------------------------------------------------------------------------------
// LOCAL HEADER FILE
//------------------------------------------------------------------------------
#include "kreport_cmdline.h"
#include "kservcli.h"
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
#define NUMBERofSENSORS	16	// 0...15
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static struct gengetopt_args_info args_info;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void _signalHandler( int sig __attribute__((__unused__)) ) {
	cmdline_parser_kreport_free(&args_info);
	_fatalError("Interrupt catched. Stopping ..");
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool R_testValuesCRC(struct R_SensorSent_t data, int port) {
	__uint64_t crc = R_computeValueCRC(data);
	if (data.crc != crc) {
		_warning("Received inconsistent data (wrong CRC) [port %d]. Ignored", port);
		return false;
	}
	return true;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool R_testTablesCRC(struct R_TableSent_t table, int port) {
	__uint64_t crc = R_computeTableCRC(table);
	if (table.crc != crc) {
		_warning("Received inconsistent data (wrong CRC) [port %d]. Ignored", port);
		return false;
	}
	return true;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static char *convert(__uint64_t timer) {
	struct tm *t;
	time_t tim;
	char *pt;

	tim = (time_t) timer;
	t = localtime(&tim);
	pt = asctime(t);
	if (pt[strlen(pt) - 1] == '\n') pt[strlen(pt) - 1] = '\0';
	return(pt);
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char *argv[]) {
	char serverID[MAXSERVERNAME];
	int socketServer;
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_kreport(argc, argv, &args_info) != 0)
		exit(EXIT_FAILURE);
	//----  Go on --------------------------------------------------------------
	signal(SIGABRT, &_signalHandler);
	signal(SIGTERM, &_signalHandler);
	signal(SIGINT, &_signalHandler);
	signal(SIGPIPE, SIG_IGN);
	strcpy(serverID, SERVER_BYDEFAULT);
	u_int16_t portID = PORT_REPORTER;
	int n;

	while (1) {
		while (1) {
			n = _connect2server(serverID, portID, &socketServer);
			if (n == 0) break;
			_warning("Cannot connect socket (%d) with Server (%s): %d. Wait 10 second", portID, serverID, n);
			_threadSleep(10000);
		}

		_log(args_info.verbose_flag, "...Request values for sensor tables\n");
		n = _sendCommand2server(socketServer, R_TABLES_REQUESTED);
		if (n != 0) _fatalError("Cannot send command (%d): %d", socketServer, n);
		fprintf(stdout, "-------------------------\n");
		fprintf(stdout, "| TABLES   | ROWS       |\n");
		fprintf(stdout, "-------------------------\n");
		struct R_TableSent_t table;
		ssize_t cr;
		do {
			if (recv(socketServer, (char *)&table, sizeof(table), 0) == -1)
				_fatalError("Error when receiving values on socket %d: %s", socketServer, strerror(errno));
			table.crc = be64toh(table.crc);
			if (R_testTablesCRC(table, portID)) {
				table.size =ntohl((uint32_t) table.size);
				table.rows = ntohl((uint32_t)table.rows);
				if (table.size != 0) fprintf(stdout, "| %-8s | %10ld |\n", table.name, table.rows);
			}
		} while (table.size != 0);
		fprintf(stdout, "-------------------------\n");

		n = _closeServer(socketServer);
		if (n != 0) _fatalError("Cannot close socket (%d): %d", socketServer, n);

		_threadSleep(5000);

		struct R_SensorSent_t data;
		for (int i = 0; i < 20; i++) {
			while (1) {
				n = _connect2server(serverID, portID, &socketServer);
				if (n == 0) break;
				_warning("Cannot connect socket (%d) with Server (%s): %d. Wait 10 second", portID, serverID, n);
				_threadSleep(10000);
			}

			unsigned int sensor = _rand_interval(0, NUMBERofSENSORS - 1);
			_log(args_info.verbose_flag, "...Request values for sensor %d\n", sensor);
			n = _sendCommand2server3(socketServer, R_VALUES_REQUESTED, sensor, (__uint64_t)-1, (__uint64_t)-1);
			if (n != 0) _fatalError("Cannot send command (%d): %d", socketServer, n);
			fprintf(stdout, "--------------------------------------------------------------\n");
			fprintf(stdout, "| Sensor %02d                                                  |\n", sensor);
			fprintf(stdout, "--------------------------------------------------------------\n");
			do {
				if (recv(socketServer, (char *)&data, sizeof(data), 0) == -1)
					_fatalError("Error when receiving values on socket %d: %s", socketServer, strerror(errno));
				data.crc = be64toh(data.crc);
				if (R_testValuesCRC(data, portID)) {
					data.sensorId =ntohl(data.sensorId);
					data.time = be64toh(data.time);
					data.value = _double_swap(data.value);
					if (data.sensorId != NOMORESENSOR)
						fprintf(stdout, "| %05d | %010ld | %-22s | %10.6f |\n", data.sensorId, data.time, convert(data.time), data.value);
				}
			} while (data.sensorId != NOMORESENSOR);
			fprintf(stdout, "--------------------------------------------------------------\n");

			n = _closeServer(socketServer);
			if (n != 0) _fatalError("Cannot close socket (%d): %d", socketServer, n);
			_threadSleep(3000);	// 3 seconds
		}
	}
	//---- Exit ----------------------------------------------------------------
	// Never reached
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
