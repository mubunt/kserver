//------------------------------------------------------------------------------
// Copyright (c) 2015, 2020, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kserver
// Prototype of a bi-threaded server to be used as test vehicule for 'kong' application.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <stdarg.h>
#include <semaphore.h>
#include <pthread.h>
#include <stdbool.h>
#include <ctype.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sqlite3.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "kservcli.h"
#include "kserver_internal.h"
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static bool _AtestCRC(unsigned int command, struct A_Sensor_t sensoruplet) {
	if (sensoruplet.crc != A_computeCRC(command, sensoruplet)) {
		_warning("Received inconsistent data (wrong CRC) [port %d]", PORT_AUTOMATON);
		return false;
	}
	return true;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int _callback(void *NotUsed, int argc, char **argv, char **azColName) {
	return 0;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _insertValuesInDatabase(struct A_Sensor_t data) {
	char tablename[64];
	char sql[256];
	char *zErrMsg = 0;
	long count = 0;

	// Name of the table associated with the sensor.
	sprintf(tablename, TABLENAMEFORMAT, data.sensorId);
	// Count the number of rows in a database table.
	sprintf(sql, "SELECT COUNT(*) FROM %s", tablename);
	// Execute SQL statement
	if (SQLITE_OK != sqlite3_exec(db, sql, _callback_count, &count, &zErrMsg)) {
		strcpy(sql, zErrMsg);
		sqlite3_free(zErrMsg);
		_sfatalError("SQL error: %s\n", sql);
	}
	//_slog(LOGNORMAL, "...[%d] %d records are already stored in table %s\n", PORT_AUTOMATON, count, tablename);
	// Insert new row
	sprintf(sql, "INSERT INTO %s (IDX, TIME, VALUE) VALUES (%ld, %ld, %f);", tablename, count, data.time, data.value);
	// Execute SQL statement
	if (SQLITE_OK != sqlite3_exec(db, sql, _callback, 0, &zErrMsg)) {
		strcpy(sql, zErrMsg);
		sqlite3_free(zErrMsg);
		_sfatalError("SQL error: %s\n", sql);
	}
	_slog(LOGNORMAL, "...[%d] Sensor %d: Record (%ld, %lld, %f) created successfully\n", PORT_AUTOMATON, data.sensorId, count, data.time, data.value);
}
//------------------------------------------------------------------------------
// MAIN FUNCTIONS
//------------------------------------------------------------------------------
void *_dataAcquisitionThread( void *arg  __attribute__((__unused__)) ) {
	SOCKET server = (SOCKET) -1;
	SOCKET client_automaton = (SOCKET) -1;
	struct sockaddr_in serv_addr;

	_slog(LOGBOLD, "Starting acquisition thread...\n");
	LOCK_DATABASE_ACCESS();
	setConnection(SERVER_BYDEFAULT, PORT_AUTOMATON, &server, &serv_addr);
	UNLOCK_DATABASE_ACCESS();

	size_t length = sizeof(serv_addr);
	while (1) {
		client_automaton = accept(server, (struct sockaddr *) &serv_addr, (socklen_t *) &length);
		if (client_automaton < 0) continue;
		acq_counter++;

		struct Command_t cmd, cmd2;
		struct A_Sensor_t sensoruplet;
		bool endofloop = false;

		while (1) {
			ssize_t rc;
			if (0 > (rc = recv(client_automaton, (void *)&cmd2, sizeof(cmd2), 0)))
				_sfatalError("Receiving data failed [port %d]", PORT_AUTOMATON);
			if (rc == 0) {
				cmd.command = SERVER_DISCONNECT;
			} else {
				cmd.command = ntohl(cmd2.command);
			}
			switch (cmd.command) {
			case A_VALUES_SENT:
				if (0 > recv(client_automaton, (void *)&sensoruplet, sizeof(sensoruplet), 0))
					_sfatalError("Receiving data failed [port %d]", PORT_AUTOMATON);
				sensoruplet.crc = be64toh(sensoruplet.crc);
				if (_AtestCRC(cmd2.command, sensoruplet)) {
					sensoruplet.sensorId = ntohl(sensoruplet.sensorId);
					sensoruplet.time = be64toh(sensoruplet.time);
					sensoruplet.value = _double_swap(sensoruplet.value);
					LOCK_DATABASE_ACCESS();
					_insertValuesInDatabase(sensoruplet);
					UNLOCK_DATABASE_ACCESS();
				}
				break;
			case SERVER_DISCONNECT:
				endofloop = true;
				break;
			default:
				_slog(LOGNORMAL, "...[%d] Received UNKNOWN command\n", PORT_AUTOMATON);
				break;
			}
			if (endofloop) break;
		}
		closesocket(client_automaton);
	}
	_slog(LOGNORMAL, "Stopping acquisition thread...\n");
	pthread_exit(0);
	return 0;
}
//------------------------------------------------------------------------------
