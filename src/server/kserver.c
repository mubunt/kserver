//------------------------------------------------------------------------------
// Copyright (c) 2015, 2020, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kserver
// Prototype of a bi-threaded server to be used as test vehicule for 'kong' application.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <signal.h>
#include <termcap.h>
#include <termios.h>
#include <stdarg.h>
#include <errno.h>
#include <semaphore.h>
#include <pthread.h>
#include <stdbool.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sqlite3.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "kserver_cmdline.h"
#include "kservcli.h"
#include "kserver_internal.h"
#include "kserver.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define FATALERROR_PREFIX					"FATAL ERROR: "
#define ESC_BOLDRED							"\033[1;31m"
#define ESC_STOP							"\033[0m"
#define EXIST(file)							(stat(file, ptlocstat)<0 ? 0:locstat.st_mode)
#define BACKLOG								5	// how many pending connections queue will hold
#define IPCKEY								getpid()	// IPC key for this client
#define	DEFAULT_LOGDIR						"/tmp"
#define LOGFILENAME 						"serverlog"
#define LOGFILESUFF							".log"

#define CREATEIPC(id, key) 					if ((id = msgget(key, S_IRWXU | IPC_CREAT)) == -1) \
												_sfatalError("Cannot create new message queue (errno='%s')", strerror(errno));
#define DELETEIPC(id)						msgctl(id, IPC_RMID, IPC_PRIVATE);
#define	WRITEIPC(id, msg)					int _n = msgsnd(id, &msg, strlen(msg.mtext), IPC_PRIVATE)
#define READIPC(id, msg) 					do { \
												ssize_t _n; \
												while (((_n = msgrcv(id, &msg, MSG_MAX_SIZE, 0, IPC_PRIVATE)) == -1) && errno == 4); \
												msg.mtext[_n] = '\0'; \
											} while (0)
#define SEND_COMMAND(id, cmd, msg)			do { \
												snprintf(msg.mtext, sizeof(msg.mtext), "%s", cmd); \
												WRITEIPC(id, msg); \
											} while (0)
#define SEND_COMMAND2(id, cmd, param, msg)	do { \
												snprintf(msg.mtext, sizeof(msg.mtext), "%s:%s", cmd, param); \
												WRITEIPC(id, msg); \
											} while (0)
#define SEND_COMMAND3(id, cmd, param, msg)	do { \
												snprintf(msg.mtext, sizeof(msg.mtext), "%s:%d", cmd, param); \
												WRITEIPC(id, msg); \
											} while (0)
//------------------------------------------------------------------------------
// EXTERNAL FUNCTIONS
//------------------------------------------------------------------------------
extern void *_dataAcquisitionThread( void * );
extern void *_dataAReportingThread( void * );
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
pthread_mutex_t						my_mutex;
sqlite3 							*db 			= '\0';
unsigned int						acq_counter		= 0;
unsigned int						rep_counter		= 0;
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static struct gengetopt_args_info	args_info;
static bool							log_requested	= true;
static FILE 						*fdlog			= NULL;
static pthread_t 					th_acquisition;
static pthread_t 					th_reporting;
static pthread_t					listeningconsole;
static int							console_request;		// request identificator for acquring
static int							console_response;		// response identificator for acquring
static s_msg						conmsg;					// IPC message
static char 						serverlogfile[PATH_MAX];
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void splitMessage(char *msg, char *key, char *additional) {
	char tmp[KEY_MAX_SIZE + MSG_MAX_SIZE + 1];
	char info[MSG_MAX_SIZE + 1];
	strncpy(tmp, msg, sizeof(tmp));
	(void) strtok(tmp, KEYSEP);
	if (msg[strlen(tmp)] == KEYSEP[0]) {
		strncpy(key, tmp, KEY_MAX_SIZE);
		strncpy(additional, tmp + strlen(key) + 1, sizeof(additional) - 1);
	} else {
		strncpy(key, tmp, KEY_MAX_SIZE);
		strcpy(additional, "");
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Cleaning context (free memory, delete IPC, etc.) before exiting
static void _cleanContextAndExit(int cptrdu) {
	_slog(LOGBOLD, "Exiting server...\n");
	cmdline_parser_kserver_free(&args_info);
	fclose(fdlog);
	exit(cptrdu);
}

static void _signalHandler( int sig __attribute__((__unused__)) ) {
	_sfatalError("Interrupt catched. Stopping ..");
}

static void getDate( char *ts ) {
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	sprintf(ts, "%02d-%02d-%02d", timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday);
}

static void getTimestamp( char *ts ) {
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	sprintf(ts, "%02d:%02d:%02d", timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void *consoleListeningThread(void *arg  __attribute__((__unused__))) {
	_slog(LOGBOLD, "Starting console thread...\n");
	char key[KEY_MAX_SIZE + 1];
	char additional_information[PATH_MAX];
	while (1) {
		conmsg.mtext[0] = '\0';
		READIPC(console_request, conmsg);
		splitMessage(conmsg.mtext, key, additional_information);

		if (strcmp(key, CMD_STOP) == 0) {
			break;
		}
		if (strcmp(key, CMD_LOGON) == 0) {
			log_requested = true;
			continue;
		}
		if (strcmp(key, CMD_LOGOFF) == 0) {
			_slog(LOGNORMAL, "Command LOG OFF sent to server...\n");
			fflush(fdlog);
			log_requested = false;
			continue;
		}
		if (strcmp(key, CMD_ACQ) == 0) {
			SEND_COMMAND3(console_response, CMD_ACQ, acq_counter, conmsg);
			continue;
		}
		if (strcmp(key, CMD_REP) == 0) {
			SEND_COMMAND3(console_response, CMD_REP, rep_counter, conmsg);
			continue;
		}
		if (strcmp(key, CMD_LOG) == 0) {
			SEND_COMMAND2(console_response, CMD_LOG, serverlogfile, conmsg);
			continue;
		}
	}
	DELETEIPC(console_request);
	DELETEIPC(console_response);
	_slog(LOGBOLD, "Stopping server...\n");
	_cleanContextAndExit(EXIT_FAILURE);
	// Never reached
	pthread_exit(NULL);
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void _slog(bool bold, const char *format, ...) {
	if (log_requested) {
		va_list argp;
		char buff[512];
		va_start(argp, format);
		vsprintf(buff, format, argp);
		if (bold)
			fprintf(fdlog, ESC_BOLDRED "%s" ESC_STOP, buff);
		else
			fprintf(fdlog, "%s", buff);
		fflush(fdlog);
		va_end(argp);
	}
}

void _sfatalError(const char *msg, ...) {
	va_list al;
	char buff[512];

	strcpy(buff, "\n");
	strcat(buff, FATALERROR_PREFIX);
	va_start(al, msg);
	(void) vsprintf(buff + strlen(FATALERROR_PREFIX) + 1, msg, al);
	(void) strcat(buff, ".\n\n");
	va_end(al);
	fprintf(stderr, ESC_BOLDRED "%s" ESC_STOP, buff);
	_slog(LOGNORMAL, ESC_BOLDRED "%s" ESC_STOP "\n", buff);
	_cleanContextAndExit(EXIT_FAILURE);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void setConnection(const char *serverip, uint16_t port, SOCKET *server, struct sockaddr_in *serv_addr) {
	*server = socket(AF_INET, SOCK_STREAM, 0);
	if (*server < 0)
		_sfatalError("Opening socket failed [port %d]", port);
	_slog(LOGNORMAL, "...[%d] Socket opened\n", port);
	(void) bzero((char *) serv_addr, sizeof(*serv_addr));
	serv_addr->sin_family = AF_INET;
	serv_addr->sin_addr.s_addr = inet_addr(serverip);
	serv_addr->sin_port = htons(port);
	// lose the pesky "Address already in use" error message
	int n, yes = 1;
	if ((n = setsockopt(*server, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int))) == -1) {
		close(*server);
		_sfatalError("Setsockopt failed (%d) [port %d]", n, port);
	}
	if ((n = bind(*server, (struct sockaddr *) serv_addr, sizeof(*serv_addr))) < 0) {
		close(*server);
		_sfatalError("Binding failed (%d) [port %d]", n, port);
	}
	_slog(LOGNORMAL, "...[%d] Binding socket done\n", port);
	(void) listen(*server, BACKLOG);
	_slog(LOGNORMAL, "...[%d] Listened socket\n", port);
	_slog(LOGNORMAL, "...[%d] Server is up and running\n", port);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int _callback_count( void *data, int count, char **rows, char **NotUsed ) {
	if (count == 1 && rows) {
		*(int *)data = atoi(rows[0]);
		return 0;
	}
	return 1;
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	struct stat locstat, *ptlocstat = &locstat;	// stat variables for EXIST*
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_kserver(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;

	char databaseName[MAXPATHLEN];
	strcpy(databaseName, args_info.database_arg);
	//----  Go on --------------------------------------------------------------
	signal(SIGABRT, &_signalHandler);
	signal(SIGTERM, &_signalHandler);
	signal(SIGINT, &_signalHandler);
	signal(SIGPIPE, SIG_IGN);
	// Database opening or creation
	if (! EXIST(databaseName))
		_sfatalError("%s database does not exist", databaseName);
	if (sqlite3_open(databaseName, &db))
		_sfatalError("Can't open/create database: %s", sqlite3_errmsg(db));
	// Log file: directory and name
	char *pdir;
	if (args_info.log_given)
		pdir = args_info.log_arg;
	else
		pdir= (char *)DEFAULT_LOGDIR;
	char date[64];
	getDate(date);
	sprintf(serverlogfile, "%s/%s-%s%s", pdir, LOGFILENAME, date, LOGFILESUFF);
	fdlog = fopen(serverlogfile, "a");
	if (fdlog == NULL)
		_sfatalError("Cannot create/open log file %s (%s)...\n", serverlogfile, strerror(errno));
	char timestamp[16];
	getTimestamp(timestamp);
	_slog(LOGBOLD, "\n== LOG ON %s ======\n\n", timestamp);

	acq_counter = 0;
	rep_counter = 0;
	log_requested = true;

	_slog(LOGNORMAL, "...Opened %s database successfully\n", databaseName);
	// Creation of 2 threads to communicate one with the controller (or several
	// data acquisition clients) and a second with data viewers.
	pthread_mutex_init (&my_mutex, NULL);
	_slog(LOGNORMAL, "...Mutex initialized\n");
	// Listen the console if any....
	CREATEIPC(console_request, CONSOLE_REQUEST);
	CREATEIPC(console_response, CONSOLE_RESPONSE);
	_slog(LOGNORMAL, "...IPC to console established\n");
	conmsg.mtype = IPCKEY;
	if (pthread_create(&listeningconsole, NULL, consoleListeningThread, NULL) == -1)
		_sfatalError("%s", "Cannot create console thread. Abort.");
	_slog(LOGNORMAL, "...Console thread successfully created\n");
	if (pthread_create (&th_acquisition, NULL, _dataAcquisitionThread, NULL) < 0) {
		_sfatalError("Cannot create data acquisition thread");
	}
	_slog(LOGNORMAL, "...Acquisition thread successfully created\n");
	if (pthread_create (&th_reporting, NULL, _dataAReportingThread, NULL) < 0) {
		_sfatalError("Cannot create data reporting thread");
	}
	_slog(LOGNORMAL, "...Reporting thread successfully created\n");
	while (1) {
		sleep(3600);
	}
	//---- Exit ----------------------------------------------------------------
	/* Never Reached */
	_cleanContextAndExit(EXIT_SUCCESS);
}
//------------------------------------------------------------------------------
