//------------------------------------------------------------------------------
// Copyright (c) 2015, 2020, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kserver
// Prototype of a bi-threaded server to be used as test vehicule for 'kong' application.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <stdarg.h>
#include <semaphore.h>
#include <pthread.h>
#include <stdbool.h>
#include <ctype.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sqlite3.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "kservcli.h"
#include "kserver_internal.h"
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static bool _RtestCRC(unsigned int command, struct R_Sensor_t sensoruplet) {
	if (sensoruplet.crc != R_computeCRC(command, sensoruplet)) {
		_warning("Received inconsistent data (wrong CRC) [port %d]. Ignored", PORT_REPORTER);
		return false;
	}
	return true;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int _sendValues( SOCKET socket, struct R_SensorSent_t data ) {
	ssize_t cr;
	if (0 >= (cr = send(socket, (void *)&data, sizeof(struct R_SensorSent_t), 0))) {
		_slog(LOGNORMAL, "...[%d] Error %d when sending data\n", PORT_REPORTER, cr);
		return(1);
	}
	return 0;
}

static int _sendTables( SOCKET socket, struct R_TableSent_t table ) {
	ssize_t cr;
	if (0 >= (cr = send(socket, (void *)&table, sizeof(struct R_TableSent_t), 0))) {
		_slog(LOGNORMAL, "...[%d] Error %d when sending table name\n", PORT_REPORTER, cr);
		return(1);
	}
	return 0;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _sendEndOfValues(SOCKET socket) {
	struct R_SensorSent_t databack;
	databack.sensorId = htonl(NOMORESENSOR);
	databack.time = htobe64(0ULL);
	databack.value = _double_swap(0.);
	databack.crc  = 0;	// To avoid warning from cppcheck!!!
	databack.crc = htobe64(R_computeValueCRC(databack));
	if (0 != _sendValues(socket, databack))
		_sfatalError("Sending of 'end of data' frame failed");
}

static void _sendEndOfTables(SOCKET socket) {
	struct R_TableSent_t table;
	for (int i = 0; i < MAXTABLENAME; i++) table.name[i] = '\0';
	table.size = table.rows = htonl(0);
	table.crc  = 0;	// To avoid warning from cppcheck!!!
	table.crc = htobe64(R_computeTableCRC(table));
	if (0 != _sendTables(socket, table))
		_sfatalError("Sending of 'end of table' frame failed");
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int _callback_values(void *socket, int argc, char **argv, char **NotUsed) {
	struct R_SensorSent_t data;
	data.sensorId = htonl((uint32_t) strtol(argv[0], NULL, 0));
	data.time = htobe64((__uint64_t) strtol(argv[1], NULL, 0));
	data.value = _double_swap(strtod(argv[2], NULL));
	data.crc  = 0;	// To avoid warning from cppcheck!!!
	data.crc = htobe64(R_computeValueCRC(data));
	return(_sendValues(*(SOCKET *)socket, data));
}

static int _callback_tables(void *socket, int argc, char **argv, char **NotUsed) {
	struct R_TableSent_t table;
	char sql[256];
	char *zErrMsg = 0;
	if (strlen(argv[0]) > (MAXTABLENAME - 1)) {
		strncpy(table.name, argv[0], (size_t) (MAXTABLENAME - 1));
		table.name[MAXTABLENAME - 1] = '\0';
	} else
		strcpy(table.name, argv[0]);
	table.size = strlen(table.name);
	// Count the number of rows in theble.
	sprintf(sql, "SELECT COUNT(*) FROM %s", argv[0]);
	// Execute SQL statement
	if (SQLITE_OK != sqlite3_exec(db, sql, _callback_count, &table.rows, &zErrMsg)) {
		strcpy(sql, zErrMsg);
		sqlite3_free(zErrMsg);
		_sfatalError("SQL error: %s\n", sql);
	}
	table.size = htonl((uint32_t)table.size);
	table.rows = htonl((uint32_t)table.rows);
	table.crc = htobe64(R_computeTableCRC(table));
	return(_sendTables(*(SOCKET *)socket, table));
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _sendValuesFromDatabase(SOCKET socket, struct R_Sensor_t data) {
	char tablename[64];
	char sql[256];
	char *zErrMsg = 0;
	// Name of the table associated with the sensor.
	sprintf(tablename, TABLENAMEFORMAT, data.sensorId);
	if ((data.timestart == -1ULL) && (data.timeend == -1ULL)) {
		sprintf(sql, "SELECT IDX, TIME, VALUE FROM %s;", tablename);
	} else {
		if (data.timestart == -1ULL)
			sprintf(sql, "SELECT IDX, TIME, VALUE FROM %s WHERE TIME <= %ld;", tablename, data.timeend);
		else {
			if (data.timeend == -1ULL)
				sprintf(sql, "SELECT IDX, TIME, VALUE FROM %s WHERE TIME >= %ld;", tablename, data.timestart);
			else
				sprintf(sql, "SELECT IDX, TIME, VALUE FROM %s WHERE TIME >= %ld AND TIME <= %ld;", tablename, data.timestart, data.timeend);
		}
	}
	// Execute SQL statement
	if (SQLITE_OK != sqlite3_exec(db, sql, _callback_values, &socket, &zErrMsg)) {
		strcpy(sql, zErrMsg);
		sqlite3_free(zErrMsg);
		_sendEndOfValues(socket);
		_sfatalError("SQL error: %s\n", sql);
	}
	_sendEndOfValues(socket);
	_slog(LOGNORMAL, "...[%d] Sensor %d: data selected successfully\n", PORT_REPORTER, data.sensorId);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _sendTablesFromDatabase(SOCKET socket) {
	char sql[256];
	char *zErrMsg = 0;

	// SQL statement
	strcpy(sql, "SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;");
	// Execute SQL statement
	if (SQLITE_OK != sqlite3_exec(db, sql, _callback_tables, &socket, &zErrMsg)) {
		strcpy(sql, zErrMsg);
		sqlite3_free(zErrMsg);
		_sendEndOfTables(socket);
		_sfatalError("SQL error: %s\n", sql);
	}
	_sendEndOfTables(socket);
	_slog(LOGNORMAL, "...[%d] Tables sent successfully\n", PORT_REPORTER);
}
//------------------------------------------------------------------------------
// MAIN FUNCTIONS
//------------------------------------------------------------------------------
void *_dataAReportingThread ( void *arg  __attribute__((__unused__)) ) {
	SOCKET server = (SOCKET) -1;
	SOCKET client_reporter = (SOCKET) -1;
	struct sockaddr_in serv_addr;

	_slog(LOGBOLD, "Starting reporting thread...\n");
	LOCK_DATABASE_ACCESS();
	setConnection(SERVER_BYDEFAULT, PORT_REPORTER, &server, &serv_addr);
	UNLOCK_DATABASE_ACCESS();

	size_t length = sizeof(serv_addr);
	while (1) {
		client_reporter = accept(server, (struct sockaddr *) &serv_addr, (socklen_t *) &length);
		if (client_reporter < 0) continue;
		rep_counter++;

		struct Command_t cmd, cmd2;
		struct R_Sensor_t sensoruplet;
		bool endofloop = false;

		while (1) {
			ssize_t rc;
			if (0 > (rc = recv(client_reporter, (void *)&cmd2, sizeof(cmd), 0)))
				_sfatalError("Receiving data failed [port %d]", PORT_REPORTER);
			if (rc == 0) {
				cmd.command = SERVER_DISCONNECT;
			} else {
				cmd.command = ntohl(cmd2.command);
			}
			switch (cmd.command) {
			case R_VALUES_REQUESTED:
				if (0 > recv(client_reporter, (void *)&sensoruplet, sizeof(sensoruplet), 0))
					_sfatalError("Receiving data failed [port %d]", PORT_REPORTER);
				sensoruplet.crc = be64toh(sensoruplet.crc);
				if (_RtestCRC(cmd2.command, sensoruplet)) {
					sensoruplet.sensorId = ntohl(sensoruplet.sensorId);
					sensoruplet.timestart = be64toh(sensoruplet.timestart);
					sensoruplet.timeend = be64toh(sensoruplet.timeend);
					LOCK_DATABASE_ACCESS();
					_sendValuesFromDatabase(client_reporter, sensoruplet);
					UNLOCK_DATABASE_ACCESS();
				} else
					_sendEndOfValues(client_reporter);
				break;
			case R_TABLES_REQUESTED:
				LOCK_DATABASE_ACCESS();
				_sendTablesFromDatabase(client_reporter);
				UNLOCK_DATABASE_ACCESS();
				break;
			case SERVER_DISCONNECT:
				endofloop = true;
				break;
			default:
				_slog(LOGNORMAL, "...[%d] Received UNKNOWN command (0x%08x)\n", PORT_REPORTER, cmd.command);
				break;
			}
			if (endofloop) break;
		}
		closesocket(client_reporter);
	}
	_slog(LOGNORMAL, "Stopping reporting thread...\n");
	pthread_exit(0);
	return 0;
}
//------------------------------------------------------------------------------
