//------------------------------------------------------------------------------
// Copyright (c) 2015, 2020, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kserver
// Prototype of a bi-threaded server to be used as test vehicule for 'kong' application.
//------------------------------------------------------------------------------
#ifndef KSERVERINTERNAL_H
#define KSERVERINTERNAL_H
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define LOCK_DATABASE_ACCESS()			pthread_mutex_lock (&my_mutex)
#define UNLOCK_DATABASE_ACCESS()		pthread_mutex_unlock (&my_mutex)

#define LOGNORMAL						0
#define LOGBOLD							1
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES DEFINED IN kserver.c
//------------------------------------------------------------------------------
extern pthread_mutex_t	my_mutex;
extern sqlite3 			*db;
extern unsigned int		acq_counter;
extern unsigned int		rep_counter;
extern bool 			stopthread;
//------------------------------------------------------------------------------
// EXTERNAL FUNCTIONS DEFINED IN kserver.c
//------------------------------------------------------------------------------
extern void _sfatalError(const char *, ...);
extern void _slog(bool, const char *format, ...);

extern void setConnection( const char *, uint16_t, SOCKET *, struct sockaddr_in * );
extern int _callback_count( void *, int, char **, char ** );
//------------------------------------------------------------------------------
#endif	// KSERVERINTERNAL_H