//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kserver
// Prototype of a bi-threaded server to be used as test vehicule for 'kong' application.
//------------------------------------------------------------------------------
#ifndef DEFKSERVER_H
#define DEFKSERVER_H
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define CONSOLE_REQUEST		0x00012343
#define CONSOLE_RESPONSE	0x00012344
#define ACQUIRING_REQUEST	0x00012345
#define ACQUIRING_RESPONSE	0x00012346
#define REPORTING_REQUEST	0x00012347
#define REPORTING_RESPONSE	0x00012348

#define MSG_MAX_SIZE		PATH_MAX	// IPC message
#define KEY_MAX_SIZE		16			// Key (part of IPC message)

#define KEYSEP				":"

#define CMD_ACQ				"ACQ"
#define CMD_REP				"REP"
#define CMD_STOP			"STOP"
#define CMD_LOG 			"LOG"
#define CMD_LOGON			"LOGON"
#define CMD_LOGOFF			"LOGOFF"
//------------------------------------------------------------------------------
// STRUCTURE DEFINITIONS
//------------------------------------------------------------------------------
// IPC message structure
typedef struct {
	long	mtype;
	char	mtext[KEY_MAX_SIZE + MSG_MAX_SIZE + 1];
} s_msg;
//------------------------------------------------------------------------------
#endif	// DEFKSERVER_H