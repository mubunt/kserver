///------------------------------------------------------------------------------
// Copyright (c) 2015, 2020, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kserver
// Prototype of a bi-threaded server to be used as test vehicule for 'kong' application.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <sqlite3.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
//------------------------------------------------------------------------------
// LOCAL HEADER FILE
//------------------------------------------------------------------------------
#include "DBcreate_cmdline.h"
#include "kservcli.h"
//------------------------------------------------------------------------------
// CONSTANTS
//------------------------------------------------------------------------------
#define NUMBERofTABLES		16
#define SQLSTATEMENT		"CREATE TABLE SENSOR%02ld(IDX INT PRIMARY KEY NOT NULL, TIME INT NOT NULL, VALUE REAL NOT NULL) WITHOUT ROWID; "

#define EXIST(file)			(stat(file, ptlocstat)<0 ? 0:locstat.st_mode)
//------------------------------------------------------------------------------
// LOCAL ROUTINES
//------------------------------------------------------------------------------
static int callback(void *NotUsed, int argc, char **argv, char **azColName) {
	int i;
	for(i = 0; i < argc; i++)
		fprintf(stdout, "%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	fprintf(stdout, "\n");
	return 0;
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char *argv[]) {
	struct gengetopt_args_info args_info;
	struct stat locstat, *ptlocstat = &locstat;	// stat variables for EXIST*
	char databaseName[MAXPATHLEN];
	size_t  sensors = 0;
	char buff[512];
	sqlite3 *db;
	char *sql;
	char *zErrMsg = 0;
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_DBcreate(argc, argv, &args_info) != 0)
		exit(EXIT_FAILURE);

	strcpy(databaseName, args_info.database_arg);
	sensors = NUMBERofTABLES;
	// Database opening or creation
	if (EXIST(databaseName))
		_fatalError("%s database already exists", databaseName);
	if (sqlite3_open(databaseName, &db)) {
		_fatalError("Can't open/create database: %s\n", sqlite3_errmsg(db));
	}
	_log(args_info.verbose_flag, "...Create %s database successfully for %d sensors\n", databaseName, sensors);
	// SQL statement
	size_t n = (strlen(SQLSTATEMENT) + 5) * sensors;
	if ((sql = malloc(n)) == NULL) {
		_fatalError("Needs in memory exceed what it is available");
	}
	*sql = '\0';
	for (size_t i = 0; i < sensors; i++) {
		sprintf(buff, SQLSTATEMENT, i);
		strcat(sql, buff);
	}
	// Execute SQL statement
	if( SQLITE_OK != sqlite3_exec(db, sql, callback, 0, &zErrMsg)) {
		_fatalError("Can't create %s database: SQL error: %s\n", databaseName, zErrMsg);
	}
	_log(args_info.verbose_flag, "...Create %d tables successfully\n", sensors);
	// Close database
	sqlite3_close(db);
	//---- Exit ----------------------------------------------------------------
	free(sql);
	cmdline_parser_DBcreate_free(&args_info);
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
