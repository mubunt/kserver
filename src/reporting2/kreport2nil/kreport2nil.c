//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kserver
// Prototype of a bi-threaded server to be used as test vehicule for 'kong' application.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "kreport2.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define CLEAR_ENTIRE_SCREEN()	fprintf(stdout, "\033[2J")
#define MOVE_CURSOR(x, y)		fprintf(stdout, "\033[%d;%df", x, y)
#define HIDE_CURSOR()			fprintf(stdout, "\033[?25l")
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void screen_mode_setRawMode( void ) {
	struct termios mode;
	tcgetattr(STDIN_FILENO, &mode);
	cfmakeraw(&mode);
	tcsetattr(STDIN_FILENO, TCSANOW, &mode);
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main() {
	screen_mode_setRawMode();
	HIDE_CURSOR();
	CLEAR_ENTIRE_SCREEN();
	for (int i = 0; i < NUMBERofSENSORS; i++) {
		MOVE_CURSOR(i * 2 + 1, 1);
		fprintf(stdout, "Sensor %02d |", i);
		MOVE_CURSOR(i * 2 + 2, 0);
		fprintf(stdout, "          |");
	}
	fflush(stdout);
	//---- Infinite loop -------------------------------------------------------
	while (1)
		sleep(3600);
	//---- Exit ----------------------------------------------------------------
	// Never reached !
	return(EXIT_SUCCESS);
}
//------------------------------------------------------------------------------
