//------------------------------------------------------------------------------
// Copyright (c) 2015, 2020, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kserver
// Prototype of a bi-threaded server to be used as test vehicule for 'kong' application.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <limits.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//------------------------------------------------------------------------------
// LOCAL HEADER FILE
//------------------------------------------------------------------------------
#include "kreport2_cmdline.h"
#include "kservcli.h"
#include "kreport2.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define TITLE 				"Data Reporting"
#define NIL					"kreport2nil"
#define LINES_WINDOW		NUMBERofSENSORS * 2
#define MAX 				100
#define COLUMNS_WINDOW		(MAX + 20)
#define FORECOLOR			"#DCDCDC"	// Gainsboro
#define BACKCOLOR			"#003333"	// DarkerTeal
#define COMMAND_WINDOW		"xfce4-terminal" \
								" --geometry=%dx%d+0+0" \
								" --color-bg=" BACKCOLOR \
								" --color-text=" FORECOLOR \
								" --title=\"%s\"" \
								" --hide-menubar" \
								" --hide-toolbar" \
								" --hide-scrollbar" \
								" --command=\"bash -c \\\"export PATH=%s; export LINES=%d; export COLUMNS=%d; %s\\\"\""
#define FORMAT_PIDOF		"pidof -x %s"
#define BLOCK				"█"

#define RED_COLOR			"\033[31m"
#define YELLOW_COLOR		"\033[33m"
#define WHITE_COLOR			"\033[37m"
#define STOP_COLOR			"\033[0m"

#define MOVE_CURSOR_AND_CLEAR_ENTIRE_LINE(x, y)		fprintf(fdevice, "\033[%d;%dH\033[K", x, y)
#define MOVE_CURSOR_AND_DISPLAY(f, v, x, y)			fprintf(fdevice, "\033[%d;%df" f, x, y, v)
#define MOVE_CURSOR_AND_DISPLAY_BLOCK(c, x, y)		fprintf(fdevice, "\033[%d;%df" c BLOCK STOP_COLOR, x, y)
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static struct gengetopt_args_info	args_info;
static FILE 						*fdevice = NULL;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static pid_t getPid( const char *process ) {
	char command[256];
	snprintf(command, sizeof(command), FORMAT_PIDOF, process);
	FILE *fdcmd = NULL;
	if (NULL == (fdcmd = popen(command, "r")))
		_fatalError("%s", "Cannot run 'pidof' command. Abort.");
	char spid[128];
	char *t = fgets(spid, sizeof(spid) - 1, fdcmd);
	pclose(fdcmd);
	if (t == NULL) return -1;
	return (pid_t) strtol(spid, (char **)NULL, 10);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _signalHandler( int sig __attribute__((__unused__)) ) {
	cmdline_parser_kreport2_free(&args_info);
	if (fdevice != NULL) {
		fclose(fdevice);
		pid_t pid = getPid(NIL);
		if (pid != -1) kill(pid, SIGTERM);
	}
	_fatalError("Interrupt catched. Stopping ..");
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool R_testValuesCRC(struct R_SensorSent_t data, int port) {
	__uint64_t crc = R_computeValueCRC(data);
	if (data.crc != crc) {
		_warning("Received inconsistent data (wrong CRC) [port %d]. Ignored", port);
		return false;
	}
	return true;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void viewWindow( void ) {
	const char *path = getenv("PATH");
	char *ptcommand = malloc(strlen(COMMAND_WINDOW) + strlen(NIL) + strlen(TITLE) + strlen(path) + 128);
	if (ptcommand == NULL)
		_fatalError("%s", "Cannot allocate memory for terminal command.");
	sprintf(ptcommand, COMMAND_WINDOW, COLUMNS_WINDOW, LINES_WINDOW, TITLE, path, LINES_WINDOW, COLUMNS_WINDOW, NIL);
	if (system(ptcommand) != 0)
		_fatalError("Error during execution of '%s'.", ptcommand);
	free(ptcommand);
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char *argv[]) {
	char serverID[MAXSERVERNAME];
	int socketServer;
	int sensors[NUMBERofSENSORS];
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_kreport2(argc, argv, &args_info) != 0)
		exit(EXIT_FAILURE);
	//----  Go on --------------------------------------------------------------
	signal(SIGABRT, &_signalHandler);
	signal(SIGTERM, &_signalHandler);
	signal(SIGINT, &_signalHandler);
	signal(SIGPIPE, SIG_IGN);
	strcpy(serverID, SERVER_BYDEFAULT);
	u_int16_t portID = PORT_REPORTER;
	int n;

	viewWindow();
	pid_t pid = getPid(NIL);
	if (pid == -1)
		_fatalError("Cannot get detached window process pid (%s)...\n", strerror(errno));
	char procPath[64];
	char device[128];
	sprintf(procPath, "/proc/%d/fd/0", pid);
	long int ret = readlink(procPath, device, 128);
	device[ret] = '\0';
	fdevice = fopen(device, "w");
	if (fdevice == NULL) {
		_fatalError("Cannot open remote device (%s)...\n", strerror(errno));
	}

	memset(sensors, 0, NUMBERofSENSORS * sizeof(int));

	while (1) {
		struct R_SensorSent_t data;
		while (1) {
			n = _connect2server(serverID, portID, &socketServer);
			if (n == 0) break;
			_warning("Cannot connect socket (%d) with Server (%s): %d. Wait 10 second", portID, serverID, n);
			_threadSleep(10000);
		}

		int sensor = (int)_rand_interval(0, NUMBERofSENSORS - 1);
		_log(args_info.verbose_flag, "...Request values for sensor %d\n", sensor);
		double value = 3.1416 * (sensor + 1);
		n = _sendCommand2server3(socketServer, R_VALUES_REQUESTED, (unsigned int)sensor, (__uint64_t)-1, (__uint64_t)-1);
		if (n != 0) _fatalError("Cannot send command (%d): %d", socketServer, n);

		int l = sensor * 2 + 1;
		int c = 12;
		int counter = 0;
		unsigned int id;
		bool force = false;
		do {
			if (recv(socketServer, (char *)&data, sizeof(data), 0) == -1)
				_fatalError("Error when receiving values on socket %d: %s", socketServer, strerror(errno));
			data.crc = be64toh(data.crc);
			id = ntohl(data.sensorId);
			if (R_testValuesCRC(data, portID)) {
				if (id != NOMORESENSOR) {
					++counter;
					if (counter == MAX + 1) {
						MOVE_CURSOR_AND_CLEAR_ENTIRE_LINE(l, c);
						force = true;
					}
					if (counter == sensors[sensor] && ! force) {
						MOVE_CURSOR_AND_CLEAR_ENTIRE_LINE(l, c + (counter % MAX) + 1);
					}
					if (counter > sensors[sensor] || force) {
						data.value = _double_swap(data.value);
						if (value == data.value) {
							if (force)
								MOVE_CURSOR_AND_DISPLAY_BLOCK(YELLOW_COLOR, l, c + ((counter > MAX) ? (counter % MAX) : counter));
							else
								MOVE_CURSOR_AND_DISPLAY_BLOCK(WHITE_COLOR, l, c + ((counter > MAX) ? (counter % MAX) : counter));
						} else {
							MOVE_CURSOR_AND_DISPLAY_BLOCK(RED_COLOR, l, c + ((counter > MAX) ? (counter % MAX) : counter));
						}
					}
					fflush(fdevice);
				}
			}
		} while (id != NOMORESENSOR);
		sensors[sensor] = counter;
		MOVE_CURSOR_AND_DISPLAY(" %d", counter, l, c + ((counter > MAX) ? (counter % MAX) : counter) + 1);
		fflush(fdevice);

		n = _closeServer(socketServer);
		if (n != 0) _fatalError("Cannot close socket (%d): %d", socketServer, n);
		_threadSleep(2000);	// 2 seconds
	}
	//---- Exit ----------------------------------------------------------------
	// Never reached
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
