//------------------------------------------------------------------------------
// Copyright (c) 2015, 2020, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: kserver
// Prototype of a bi-threaded server to be used as test vehicule for 'kong' application.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <limits.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//------------------------------------------------------------------------------
// LOCAL HEADER FILE
//------------------------------------------------------------------------------
#include "kautomaton_cmdline.h"
#include "kservcli.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define NUMBERofSENSORS	16		// 0...15
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static struct gengetopt_args_info args_info;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void _signalHandler( int sig __attribute__((__unused__)) ) {
	cmdline_parser_kautomaton_free(&args_info);
	_fatalError("Interrupt catched. Stopping ..");
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char *argv[]) {
	uint16_t portID;
	char serverID[MAXSERVERNAME];
	int socketServer;
	int Record[NUMBERofSENSORS];
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_kautomaton(argc, argv, &args_info) != 0)
		exit(EXIT_FAILURE);
	//----  Go on --------------------------------------------------------------
	signal(SIGABRT, &_signalHandler);
	signal(SIGTERM, &_signalHandler);
	signal(SIGINT, &_signalHandler);
	signal(SIGPIPE, SIG_IGN);
	strcpy(serverID, SERVER_BYDEFAULT);
	portID = PORT_AUTOMATON;

	for (int i = 0; i < NUMBERofSENSORS; i++) {
		Record[i] = -1;
	}

	while (1) {
		while (1) {
			int n;
			if (0 == (n = _connect2server(serverID, portID, &socketServer))) break;
			_warning("Cannot connect socket (%d) with Server (%s): %d. Wait 10 second", portID, serverID, n);
			_threadSleep(10000);
		}

		unsigned int sensor = _rand_interval(0, NUMBERofSENSORS - 1);
		double value = 3.1416 * (sensor + 1);
		__uint64_t timer = (__uint64_t)time(NULL);
		Record[sensor]++;
		_log(args_info.verbose_flag, "...Sensor %d: Send record %d (%lld, %f)\n",
		     sensor, Record[sensor], timer, value);
		int n = _sendCommand2server2(socketServer, A_VALUES_SENT, sensor, timer, value);
		if (n != 0) _fatalError("Cannot send command (%d): %d", socketServer, n);

		n = _closeServer(socketServer);
		if (n != 0) _fatalError("Cannot close socket (%d): %d", socketServer, n);
		_threadSleep(2000);	// 2 seconds
	}
	//---- Exit ----------------------------------------------------------------
	// Never reached
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
